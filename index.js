/* 
1. Тому що ввод тексту може здійснюватись за допомогою голосового асистенту, або вставлення тексту за допомогою миші.
Тому краще за все зчитувати значення інпута не через код кожної літери на клавіатурі, а через input.value
*/

const buttons = document.querySelectorAll('.btn')

document.addEventListener('keyup', (e) => {
  buttons.forEach((button) => 
  e.code === `Key${button.textContent}` || e.key === button.textContent 
  ? button.style.backgroundColor = 'blue' 
  : button.style.backgroundColor = 'black'
  )
})

